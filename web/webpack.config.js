const path = require('path');

module.exports = {
  mode: "development",
  entry: './src/index.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist'),
  },
  devServer: {
    static: "./dist"
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'ify-loader'
      }
    ]
  },
  resolve: {
   fallback: { 
       path: require.resolve("path-browserify"),
       crypto: require.resolve("crypto-browserify"),
       fs: false,
       buffer: false,
       stream: false
   }
 },
};
