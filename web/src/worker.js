import createModule from "./treevodevo.mjs";
import {params, params_defaults} from "./params.js";

function formatTree(tree, light_grid) {
  let js_rep = [];

  for (let i = 0; i < light_grid.size(); i++) {
    js_rep.push([]);
    
    for (let j = 0; j < light_grid.get(i).size(); j++) {
      let tree_val = Math.abs(tree.get(i).get(j)); // TODO: visualize dead cells?
      if (tree_val != 0) {
        let individual_val;
        if (tree_val < 400) {
          individual_val = -3.0;
        } else if (tree_val >= 400 && tree_val < 800) {
          individual_val = -2.0;
        } else {
          individual_val = -1.0;
        }
        js_rep[i].push(individual_val);
      } else {
        js_rep[i].push(light_grid.get(i).get(j) + 3);
      }
    }
  }

  return js_rep;
}

createModule().then((Module) => {
  let pop;

  let PARAMS = {};

  for (const [i, p] of params.entries()) {
    PARAMS[p] = params_defaults[i];
  }

  self.onmessage = ({data: { action, data }}) => {
    switch (action) {
      case "update_params":
        PARAMS = data["params"];
        break;
      case "start_sim":
        console.log(PARAMS);

        if (pop) {
          pop.delete();
        }

        pop = new Module.Population();
        pop.SetConfigurableParameters(
          PARAMS["nutrient_uptake"], PARAMS["light_uptake"], PARAMS["under_shade"],
          PARAMS["n_trees"], PARAMS["fitness"], PARAMS["custom_fit_a"],
          PARAMS["custom_fit_b"], PARAMS["custom_fit_c"],
          PARAMS["mutation_rate"]
        );
        pop.InitPopulation();
        for (let n = 0; n <= (PARAMS["generations"]/5); n++) {
          let t = n*5;
          pop.Step_FullDevelopmentN(t, 5); 

          let fittest_idx = pop.GetFittestIdx();

          // get and update fitness
          const fwh = pop.GetFitnessWidthHeight(fittest_idx);
          let fittest_fitness = fwh.get(0);
          let width = fwh.get(1);
          let height = fwh.get(2);
          self.postMessage({
            action: "update_fitness_over_time",
            data: {fitness: fittest_fitness, height: height/width, gen: t}
          });

          if (t % 100 == 0) {
            // get and show tree
            let light_grid = pop.GetLightGrid(fittest_idx);
            let tree = pop.GetTree(fittest_idx);
            let js_rep = formatTree(tree, light_grid);

            let fitnesses = pop.GetEnvFitnesses(fittest_idx);
            let env_fitnesses = [];
            for (let i = 0; i < fitnesses.size(); i++) {
              env_fitnesses.push(fitnesses.get(i));
            }

            let fitness_dist = pop.GetFitnessDist();
            let fitness_dists = [];
            for (let i = 0; i < fitness_dist.size(); i++) {
              fitness_dists.push(fitness_dist.get(i));
            }

            let population = pop.GetTreePopulation();
            let js_population = []
            for (let i = 0; i < population.size(); i++) {
              let pop_i = population.get(i);
              let grid_i = formatTree(pop_i.cell_grid, pop_i.light_grid);
              let fitnesses_i = [];
              for (let j = 0; j < pop_i.fitness.size(); j++) {
                fitnesses_i.push(pop_i.fitness.get(j));
              }
              js_population.push([grid_i, fitnesses_i]);
            }

            let genotype = pop.GetGenotype(fittest_idx);

            self.postMessage({
              action: "update_current_phenotype",
              data: {
                grid: js_rep,
                gen: t,
                env_fitnesses: env_fitnesses,
                fitness_dist: fitness_dists,
                population: js_population,
                genotype: genotype
              }
            });
          }
        }

        self.postMessage({
          action: "run_completed",
          data: {}
        });
        break;
    } 
  };
});
