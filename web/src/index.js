import Plotly from "plotly.js/dist/plotly";
import {params, params_defaults} from "./params.js";
import {mean, stdev, shuffle} from "./util.js";
import * as d3 from "d3";
import {graphviz} from "d3-graphviz";

const SHOW_N_POPULATION = 15;

function resetParameterValues() {
  for (const [i, param] of params.entries()) {
    // special case
    if (param === "n_trees") {
      document.querySelector(".default-radio").checked = true;
      continue;
    }

    if (param === "fitness") {
      document.querySelector(".default-radio-fitness").checked = true;
      continue;
    }

    const slider = document.querySelector(`#param_${param}`);
    const value = document.querySelector(`#param_${param}_value`);

    // initialize
    slider.value = params_defaults[i];
    if (param === "generations") {
      value.innerHTML = params_defaults[i];
    } else {
      value.innerHTML = params_defaults[i].toFixed(2);
    }

    slider.oninput = function() {
      let n = 2;
      if (param === "custom_fit_a" || param === "custom_fit_b" || param === "custom_fit_c") {
        n = 1;
      }
      value.innerHTML = parseFloat(this.value).toFixed(n);
    }
  }
}

const plotConfig = {
  displayModeBar: false
};

function resetTimePlots() {
  const plots = {
    "fitness_graph": {
      ytitle: "Best Fitness",
      color: "rgb(1, 126, 88)"
    },
    "height_graph": {
      ytitle: "Height/Width<br>Ratio",
      color: "rgb(183, 66, 0)"
    }
  };

  for (const [plot_id, config] of Object.entries(plots)) {
    const plot = {
      x: [0],
      y: [0],
      mode: "lines",
      line: {
        width: 2.25,
        color: config.color
      }
    }

    Plotly.newPlot(plot_id, [plot], {
      paper_bgcolor: 'rgba(0,0,0,0)',
      plot_bgcolor: 'rgba(255,255,255,1)',
      xaxis: {
        title: {
          text: "Generation"
        }
      },
      yaxis: {
        title: {
          text: config.ytitle
        }
      },
      margin: {
        t: 0,
        b: 40,
        l: 60,
        r: 0
      },
      font: {
        family: "Helvetica",
        size: 14
      }
    }, plotConfig);
  }

  Plotly.newPlot("fitness_dist", [{
    x: [],
    type: 'histogram',
    histnorm: 'probability',
    marker: {
      color: "rgb(1, 126, 88)"
    }
  }], {
    paper_bgcolor: 'rgba(0,0,0,0)',
    plot_bgcolor: 'rgba(255,255,255,1)',
    font: {
      family: "Helvetica",
      size: 13
    },
    xaxis: {
      title: {
        text: "Fitness"
      }
    },
    yaxis: {
      title: {
        text: "Frequency"
      }
    },
    margin: {
      t: 0,
      b: 40,
      l: 50,
      r: 0
    },
  }, plotConfig);
}

let gridData = [];
for (let i = 0; i < 20; i++) {
  gridData.push([]);
  for (let j = 0; j < 20; j++) {
    gridData[i].push(0.04);
  }
}

const color_map = [
  [0.0, "rgb(195, 139, 0)"],
  [0.03, "rgb(195, 139, 0)"],
  [0.03, "rgb(0, 52, 194)"],
  [0.06, "rgb(0, 52, 194)"],
  [0.06, "rgb(73, 134, 3)"],
  [0.1, "rgb(73, 134, 3)"],
  [0.1, 'rgb(0, 0, 3)'],
  ['0.22222222222', 'rgb(59, 15, 111)'],
  ['0.4444444444', 'rgb(140, 41, 128)'],
  ['0.666666666', 'rgb(221, 73, 104)'],
  ['0.88888888', 'rgb(253, 159, 108)'],
  ['1.0', 'rgb(251, 252, 191)']
];

const heatmapData = {
  z: gridData,
  colorscale: color_map,
  type: "heatmap", 
  showscale: false,
  text: () => "",
  hoverinfo: "text"
};

Plotly.newPlot("current_phenotype", [heatmapData], {
  paper_bgcolor: "rgba(0,0,0,0)",
  plot_bgcolor: "rgba(255,255,255,1)",
  autorange: false,
  autosize: false,
  width: 500,
  height: 450,
  margin: {
    t: 5,
    l: 30,
    r: 0,
    b: 5
  },
  xaxis: {
    showticklabels: false,
    ticks: ""
  },
  yaxis: {
    showticklabels: false,
    ticks: ""
  },
}, plotConfig);

const population_plots = document.querySelector("#population_grid");
function resetPopulationPlots() {
  population_plots.innerHTML = "";

  for (let i = 0; i < SHOW_N_POPULATION; i++) {
    let div = document.createElement("div");
    div.id = `population_${i}`;
    /* div.classList.add(...["border-dotted"]) */

    let plot_div = document.createElement("div");
    plot_div.id = `population_${i}_plot`;

    let fitness_divs = {
      "fitness_green": ["text-[#c38b00]", "inline-block", "mr-3", "text-xs"],
      "fitness_blue": ["text-[#0034c2]", "inline-block", "mr-3", "text-xs"],
      "fitness_orange": ["text-[#498603]", "inline-block", "text-xs"]
    };

    for (const [id, classes] of Object.entries(fitness_divs)) {
      let fitness_div = document.createElement("div");
      fitness_div.id = `${id}_${i}`;
      fitness_div.classList.add(...classes);
      div.append(fitness_div);
    }

    div.append(plot_div);

    population_plots.append(div);

    Plotly.newPlot(`population_${i}_plot`, [heatmapData], {
      paper_bgcolor: "rgba(0,0,0,0)",
      plot_bgcolor: "rgba(255,255,255,1)",
      autorange: false,
      autosize: false,
      width: 140,
      height: 140,
      margin: {
        t: 0,
        l: 0,
        r: 0,
        b: 10
      },
      xaxis: {
        showticklabels: false,
        ticks: ""
      },
      yaxis: {
        showticklabels: false,
        ticks: ""
      },
    }, plotConfig);
  }
}
// worker
let worker = new Worker(new URL("./worker.js", import.meta.url));

function update_params(worker, params) {
  let newParams = {};
  for (const p of params) {
    // special case, radio button
    if (p === "n_trees" || p === "fitness") {
      continue;
    }
    newParams[p] = parseFloat(document.querySelector(`#param_${p}`).value);
  }

  // nr trees radio button
  newParams["n_trees"] = parseInt(document.querySelector('input[name="param_n_trees"]:checked').value);
  newParams["fitness"] = document.querySelector('input[name="param_fitness"]:checked').value;
  console.log(newParams);

  worker.postMessage({
    action: "update_params",
    data: {
      params: newParams
    }
  });
}

const run_button = document.querySelector("#toggle_run");
const fitness_histogram = document.querySelector("#fitness_dist");

// has to be in order that the environment fitnesses are returned from C++
const fitness_ids = ["#fitness_green", "#fitness_blue", "#fitness_orange"];

let current_phenotype = null;
let current_population = null;

function onmessage({data: { action, data }}) {
  switch (action) {
    case "update_current_phenotype":
      current_phenotype = data.grid;
      Plotly.restyle("current_phenotype", {
        z: [data.grid]
      });

      document.querySelector("#current_phenotype_title").innerHTML = `Fittest individual (Generation ${data.gen})`;

      for (let i = 0; i < data.env_fitnesses.length; i++) {
        const id = fitness_ids[i];
        document.querySelector(id).innerHTML = `<strong>Fitness:</strong> ${data.env_fitnesses[i].toFixed(1)}`;
      }

      const mean_fit = mean(data.fitness_dist);
      const stdev_fit = stdev(data.fitness_dist);

      /* Plotly.update("fitness_dist", {"x": [data.fitness_dist]}, {title: `Population avg:<br>${mean_fit.toFixed(1)} ± ${stdev_fit.toFixed(1)}`}, [0]); */
      Plotly.update("fitness_dist", {"x": [data.fitness_dist]}, {}, [0]); 

      current_population = data.population;
      shuffle(current_population);
      for (let i = 0; i < SHOW_N_POPULATION; i++) {
        Plotly.restyle(`population_${i}_plot`, {
          z: [current_population[i][0]]
        });
        for (let j = 0; j < current_population[i][1].length; j++) {
          const id = fitness_ids[j];
          document.querySelector(`${id}_${i}`).innerHTML = `<strong>F:</strong> ${current_population[i][1][j].toFixed(1)}`;
        }
      }

      graphviz("#genotype")
        .scale(0.55)
        .zoom(false)
        .renderDot(data.genotype);

      break;
    case "update_fitness_over_time":
      Plotly.extendTraces("fitness_graph", {
        x: [[data.gen]], 
        y: [[data.fitness]]
      }, [0])
      Plotly.extendTraces("height_graph", {
        x: [[data.gen]], 
        y: [[data.height]]
      }, [0])
      break;
    case "run_completed":
      run_button.classList.remove("disabled:opacity-50", "focus:outline-none");
      run_button.classList.add("hover:bg-violet-600", "bg-violet-400");
      run_button.classList.remove("hover:bg-pink-700", "bg-pink-500");
      run_button.innerHTML = "Start";
      break;
  } 
}

worker.onmessage = onmessage;

window.onload = () => {
  resetParameterValues();

  document.querySelector("#reset_params").addEventListener("click", (event) => {
    resetParameterValues();
  });

  resetTimePlots();

  resetPopulationPlots(); 

  let contour = false;
  document.querySelector("#darwin").addEventListener("click", (event) => {
    // :)
    if (!current_phenotype || !current_population) {
      return;
    }
    if (!contour) {
      Plotly.restyle("current_phenotype", {
        z: [current_phenotype],
        type: "contour",
      });

      for (let i = 0; i < SHOW_N_POPULATION; i++) {
        Plotly.restyle(`population_${i}_plot`, {
          z: [current_population[i][0]],
          type: "contour",
        });
      }
      contour = true;
    } else {
      Plotly.restyle("current_phenotype", {
        z: [current_phenotype],
        type: "heatmap"
      });
      for (let i = 0; i < SHOW_N_POPULATION; i++) {
        Plotly.restyle(`population_${i}_plot`, {
          z: [current_population[i][0]],
          type: "heatmap"
        });
      }
      contour = false;
    }
  });

  // run button
  run_button.addEventListener("click", (event) => {
    if (run_button.innerHTML == "Stopped") {
      return;
    }

    if (run_button.innerHTML == "Stop") {
      run_button.innerHTML = "Stopped";
      worker.terminate();
      return;
    }

    // start

    // reset fitness graph
    resetTimePlots();

    // reset fitness text
    for (let i = 0; i < fitness_ids.length; i++) {
      const id = fitness_ids[i];
      document.querySelector(id).innerHTML = "";
    }

    run_button.classList.add("disabled:opacity-50", "focus:outline-none");
    run_button.classList.remove("hover:bg-violet-600", "bg-violet-400");
    run_button.classList.add("hover:bg-pink-700", "bg-pink-500");
    run_button.innerHTML = "Stop";

    fitness_histogram.classList.remove("invisible");

    update_params(worker, params);

    worker.postMessage({
      action: "start_sim",
      data: {}
    });
  });

  // reset button
  const reset_button = document.querySelector("#reset_run");
  const loader = document.querySelector("#loader");

  reset_run.addEventListener("mouseup", (event) => {
    if (!(current_phenotype && current_population)) {
      return;
    }

    reset_run.value = "Resetting...";
    reset_run.disabled = true;
    loader.classList.remove("invisible");

    worker.terminate();
    worker = new Worker(new URL("./worker.js", import.meta.url));
    worker.onmessage = onmessage;

    // reset fitness graph
    resetTimePlots();

    fitness_histogram.classList.add("invisible");

    // reset phenotype plot
    Plotly.restyle("current_phenotype", {
      z: [gridData]
    });  

    // reset population plots
    resetPopulationPlots();

    document.querySelector("#current_phenotype_title").innerHTML = "Fittest individual";

    // reset fitness text
    for (let i = 0; i < fitness_ids.length; i++) {
      const id = fitness_ids[i];
      document.querySelector(id).innerHTML = "";
    }

    // reset genotype
    document.querySelector("#genotype").innerHTML = "";

    current_phenotype = null;
    current_population = null;

    // reset run button
    run_button.classList.remove("disabled:opacity-50", "focus:outline-none");
    run_button.classList.add("hover:bg-violet-600", "bg-violet-400");
    run_button.classList.remove("hover:bg-pink-700", "bg-pink-500");
    run_button.innerHTML = "Start";

    loader.classList.add("invisible");

    reset_run.value = "Reset";
    reset_run.disabled = false;
  });

  loader.classList.add("invisible");

  // menu
  const dashboard = document.querySelector("#dashboard");
  const dashboard_tab = document.querySelector("#dashboard-tab");
  const whatisthis = document.querySelector("#whatisthis");
  const whatisthis_tab = document.querySelector("#whatisthis-tab");

  const active_class_name = "inline-block p-4 text-gray-800 bg-stone-100 rounded-t-lg active";
  const inactive_class_name = "inline-block p-4 rounded-t-lg hover:text-gray-600 hover:bg-gray-50";
  dashboard_tab.addEventListener("click", (event) => {
    dashboard_tab.className = active_class_name;
    whatisthis_tab.className = inactive_class_name;

    dashboard.classList.toggle("hidden");
    whatisthis.classList.toggle("hidden");
  });

  whatisthis_tab.addEventListener("click", (event) => {
    whatisthis_tab.className = active_class_name;
    dashboard_tab.className = inactive_class_name;

    dashboard.classList.toggle("hidden");
    whatisthis.classList.toggle("hidden");
  });
}
