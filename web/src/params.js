// parameters sliders
const params = ["generations", "nutrient_uptake", "light_uptake", "under_shade",
  "n_trees", "mutation_rate", "fitness", "custom_fit_a", "custom_fit_b", "custom_fit_c"];
const params_defaults = [2000, 5, 2, 0.5, 3, 0.05, "size", 1.0, 0.0, 0.0];

export {params, params_defaults};
