#ifndef parametersHeader
#define parametersHeader

#include "Cell.hh"
#include "stdlib.h"


using namespace std;

class Parameter{
public:
    static const int width = 20;
    static const int height = 20;
    int nr_of_trees;
    int steps;
    float nutrient_influx; //[1-10]
    float energy_synthesis;//[0.5-2]
    float light_A;//light follows a gradient Ax+B
    float light_B;//light follows a gradient Ax+B
    float side_shade;//shade on cells to the side of new cell
    float under_shade;//shade on cells under new cell
    double mutate_mu;
    double mutate_mustd;
    float D_nutrients; //diffusion speed nutrients
    float D_energy;    //diffusion speed energy
    float threshold_nutrients; //division threshold
    float threshold_energy;    //division threshold
    float metabolic_rate;      //the metabolic rate
    bool celldeath; //if a metabolic need for cells is activated -> can result in celldeath
    bool neumann_neighbourhood; //if we use neumann_neighbourhood, the cell division direction is determined by the 2 output nodes as a binary nr. if Moore, then output nodes are -1, 0 or 1 for left/right, up/down
    int popsize;       //[100]
    int nrgenerations; //[100-5000]
    bool ccs; //whether there is (evolvable) cell-cell signalling
    bool direct_diffusion; //if diffusion is direct or not
    bool random_seeding; //if the initital seeding is random
    bool raytracing;
    std::string fitness;
    bool nb_old;

    //fitness parameters
    float fbiomass;
    float fwidth;
    float fheight;

    Parameter();
    ~Parameter();

    void SetParameters();
    void SetConfigurableParameters(float nutrient_influx);
};
#endif
