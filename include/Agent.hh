#ifndef AgentHeader
#define AgentHeader

#include "Cell.hh"
#include "Genome.hh"
#include "parameters.hh"
#include "stdlib.h"
#include <vector>
#include <memory>
#include "Environment.hh"
#include <string.h>
// #include <unique_ptr>

extern Parameter par;

class Agent{
    public:
    std::vector<unique_ptr<Cell>> cells;
    // std::vector<std::vector<unique_ptr<Cell>>> trees;
    int width;  //width of simulation grid
    int height; //height of simulation grid
    std::vector<std::vector<float>> lightgrid = 
      std::vector<std::vector<float>>(par.height, std::vector<float>(par.width));
    std::vector<std::vector<int>> cellgrid = 
      std::vector<std::vector<int>>(par.height, std::vector<int>(par.width));
    float fitness;
    int agentid; //for population
    int agentnr; //for in environment
    int myheight = -1;
    int mywidth = -1;

    Environment<Agent> *env;
    // std::vector<std::vector<float>> *lightgrid2;//this stores a pointer to a lightgrid noh?
    // std::vector<std::vector<float>> *cellgrid2;


    std::vector<std::vector<std::vector<float>>> recorded_lightgrids;
    std::vector<std::vector<std::vector<int>>> recorded_cellgrids;

    Genome genome;

    //Functions
    Agent();
    ~Agent();

    void InitAgent(int aid);
    void Develop(int steps); //develop agent for N steps
    void DevelopAndRecord(int steps);
    std::string coordstring(const Cell &cell);
    std::string print_AgentGraph_dot();
    std::string print_AgentGraph_edges();
    void Divide_cells();
    int GetHeight();
    int GetWidth();
    void UpdateNeighbours(Cell *cell);
    void Plant_seed(int x, int y); //function to 'plant a seed' at (x, y)
    int get_new_cellid();
    void set_environment(Environment<Agent> *environment);

    void MakeAgentFromParent(Agent &A, int agentid);

    void update_nutrients(); //cells in layer 0 get influx of nutrients and nutrients diffuse
    void diffuse_nutrients();    //cells get energy based on light, cells share energy
    void update_celldivision(); //cells can divide if nutrients + energy > threshold
    void update_lightgradient(int x, int y);//update the lightgradient after changes in morphology and new shade
    void update_intracellstate(); //update the internal node state of cells
    void print_in_terminal(string option);
    void SaveData(string file);

    
};

#endif
