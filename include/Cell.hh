#ifndef CellHeader
#define CellHeader

#include <vector>
#include "Genome.hh"


class Cell{
    public:
    int cellid;
    int parentindex;

    int coordinates[2]; //x,y
    float nutrients[3]; //soil nutrients and energy and CCS if applicable
    float diffstates[2]; //diffstates of soil nutrients and energy
    int growtype;
    bool alive = true;

    //CELLSTATE
    vector <double> sensory;
    vector <bool> regnodes; //booleans should neatly convert to ints
    vector <int> outnodes; //if we use Moore neighbourhood (neumann_neighbourhood == false) these range  between -1 and 1

    vector<Cell*> neighbours;

    //Functions
    Cell(int id, int x, int y, Genome &G);
    ~Cell();
    void Cellbirth(const Cell &Parent, int index);
    double getNeighInput(); //returns the signalling status of the neighbours (0,1 if neumann, -1,0,1 if moore)
    void update_cellstate(const Genome &G); //update the state of the cell network
};

#endif