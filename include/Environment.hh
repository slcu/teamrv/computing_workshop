#ifndef EnvironmentHeader
#define EnvironmentHeader

// #include "Population.hh"
#include <vector>
#include <cstdio>
#include <stdlib.h> 
#include "parameters.hh"
// #include "Agent.hh"

extern Parameter par;
class Agent;

template <typename T>
class Environment{
  public:
    int numtrees; //keep track of the number of trees
    std::vector<std::vector<float>> lightgrid;
    std::vector<std::vector<int>> cellgrid;

    std::vector<std::vector<std::vector<float>>> recorded_lightgrids;
    std::vector<std::vector<std::vector<int>>> recorded_cellgrids;
    
    std::vector<T*> agents;

    bool default_grid_initialized = false;
    std::vector<std::vector<float>>
      DEFAULT_LIGHT_GRID = std::vector<std::vector<float>>(par.height, std::vector<float>(par.width));

    //functions
    Environment();
    ~Environment();

    void update_lightgradient(int x, int y);
    bool plant_tree(T* tree, int x, int y); //plant a tree at coordinates x, y
    void Develop(int steps);
    void reset();
    void print_in_terminal(string option);
    //when making an agent make pointers to the lightgrid and cellgrid
};
#endif
