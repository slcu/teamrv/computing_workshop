#ifndef PopulationHeader
#define PopulationHeader

#include "Environment.hh"
#include "Agent.hh"
#include "Cell.hh"
#include "Genome.hh"
#include <cstdio>
#include <stdlib.h> 
#include <iostream>
#include <unordered_map>

struct PopulationData {
  std::vector<std::vector<int>> cell_grid;
  std::vector<std::vector<float>> light_grid;
  std::vector<float> fitness;

  PopulationData() {}
  PopulationData(
    std::vector<std::vector<int>> cell_grid,
    std::vector<std::vector<float>> light_grid,
    std::vector<float> fitness
  ) : cell_grid(cell_grid), light_grid(light_grid), fitness(fitness) {}
};

class Population
{
 public:
  int current_largest_id=0;
  
  std::unordered_map< int , Agent * > Trees;
  std::unordered_map< int , Agent * > newTrees;
//   std::unordered_map< int , Agent * > Ancestors;
  std::vector<Environment<Agent> *> environments;

//   Population();
  ~Population();
  
//   std::list<int> popancestry;
//   typedef std::list<int>::iterator iter;

  inline int NewId(void){
    current_largest_id++;
    return current_largest_id-1;
  }
  void InitPopulation();
  void Step_FullDevelopment(int Time);
  void Step_FullDevelopmentN(int Time, int N);
  int GetFittestIdx();
  float GetFitness(int idx);
  std::vector<float> GetFitnessWidthHeight(int idx);
  std::string GetGenotype(int idx);
  int GetHeight(int idx);
  int GetWidth(int idx);
  std::vector<PopulationData> GetTreePopulation();
  std::vector<float> GetFitnessDist();
  std::vector<float> GetEnvFitnesses(int agent_idx);
  std::vector<std::vector<int>> GetTree(int idx);
  std::vector<std::vector<float>> GetLightGrid(int idx);
  void SetConfigurableParameters(float nutrient_influx, float energy_synthesis,
      float under_shade, int nr_trees, std::string fitness, 
      float fbiomass, float fwidth, float fheight, float mutation_rate);

  void RecordAgent(int idx);
  std::vector<std::vector<std::vector<float>>> GetRecordedLightGrids(int idx);
  std::vector<std::vector<std::vector<int>>> GetRecordedCellGrids(int idx);

//   void InitPopulationFromGenome(char *fname);
  // void DeathOfAgent(int key);
//   void ReproduceAgent(int t,int source,int dest);
  
//   void WriteFieldToFile(int time);
//   void WritePopFitnessToFile(int time);
//   void WriteGenomeLengthToFile(int time);
//   void WriteBandsToFile(int time);
//   void WriteFittestMatricesToFile(int time);
//   void WriteAncestriesToFile(int t);
//   void ReadAncestorsFromFile(char *fname);
//   void WriteFullAncestry(Agent *C);
//   void WriteAgentToFile(char *dirname,int i, int j);
//   void threading_func(vector<int> index, int Time, int startidx);
//   vector<int> PrepDevelopment();
//   void Step_FullDevelopment_thread(int Time, int threads, vector<int> parent_keys);
//   void FinishDevelopment();
//   void OfflineAncestry(int Time); //this function ensures that the tags for ancestors are stored correctly
};
#endif
