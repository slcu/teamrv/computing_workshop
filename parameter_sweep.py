import os;

def main():
    nutrient_influx = 10
    energy_synthesis = 1
    threshold_energy = 1
    threshold_nutrients = 1
    metabolic_rate = 1
    ccs = 1
    random_seeding = 1
    mutate_mu = 1
    mutate_mustd = 1
    command = f"bin/Run {nutrient_influx} {energy_synthesis} {threshold_energy} {threshold_nutrients} {metabolic_rate} {ccs} {random_seeding} {mutate_mu} {mutate_mustd}"
    print(command)
    os.system(command)
    return

if __name__ == "__main__":
    main()
