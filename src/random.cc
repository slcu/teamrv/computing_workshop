//random.cpp
#include <stdio.h>
#include <iostream>
#include <chrono>
#include "random.hh"
#include "parameters.hh"

extern Parameter par;

XoshiroCpp::Xoshiro256PlusPlus my_rng;
//std::minstd_rand my_rng {}; // Defines an engine
std::uniform_real_distribution<double> my_unif_real_dist(0., 1.); //Define distribution
std::normal_distribution<double> normal_1_005(0.0, par.mutate_mustd);
// Function to seed the random number generator from main file
// useful if you want the seed from a parameter file
// a negative value for seed gets you a random seed
// outputs the seed itself
int Seed(int seed)
{
  if (seed < 0) {
    long rseed=static_cast<long unsigned int>(std::chrono::high_resolution_clock::now().time_since_epoch().count());
    std::cerr << "Randomizing random generator, seed is "<<rseed<<std::endl;
    my_rng = XoshiroCpp::Xoshiro256PlusPlus(seed);
    return rseed;
  } else {
    std::cerr << "User-provided seed is "<<seed<<std::endl;
    my_rng = XoshiroCpp::Xoshiro256PlusPlus(seed);
    return seed;
  }
}
// This is the function to call if you want a random number in the interval [0,1)
double uniform(void)
{
  return my_unif_real_dist(my_rng);
}

double returnNormal(double mean, double stdev)
{
  return normal_1_005(my_rng);
}
