#include "stdio.h"
#include "parameters.hh"

Parameter par;

Parameter::~Parameter(){
}

// Default parameter values specified in the constructor
Parameter::Parameter() {
    steps = 200;
    popsize=90;
    nr_of_trees = 1; //>1 for competition
    nutrient_influx=5;
    energy_synthesis=10;
    light_A = 20; 
    light_B = 5; //light follows a gradient Ax+B
    side_shade = 0.8;
    under_shade = 0.5;
    mutate_mu = 0.05;
    mutate_mustd = 0.5;
    D_nutrients = 0.2; //diffusion speed nutrients
    D_energy = 0.2;    //diffusion speed energy
    threshold_nutrients = 10; //division threshold
    threshold_energy = 5;    //division threshold
    metabolic_rate = 0.55;
    celldeath = true;
    neumann_neighbourhood = false;
    ccs = true;
    direct_diffusion = false;
    random_seeding = true;
    raytracing = true;
    fitness = "size";
    nb_old = true;
}
