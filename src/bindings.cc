#include <emscripten/bind.h>
#include "parameters.hh"
#include "Population.hh"
#include "Agent.hh"

extern Parameter par;

using namespace emscripten;

EMSCRIPTEN_BINDINGS(treevodevo) {
  register_vector<int>("vector<int>");
  register_vector<std::vector<int>>("vector<vector<int>>");
  /* register_vector<std::vector<std::vector<int>>>("vector<vector<int>>"); */

  register_vector<float>("vector<float>");
  register_vector<std::vector<float>>("vector<vector<float>>");
  /* register_vector<std::vector<std::vector<float>>>("vector<vector<float>>"); */

  value_object<PopulationData>("PopulationData")
    .field("cell_grid", &PopulationData::cell_grid)
    .field("light_grid", &PopulationData::light_grid)
    .field("fitness", &PopulationData::fitness)
    ;

  register_vector<PopulationData>("vector<PopulationData>");

  class_<Population>("Population")
    .constructor()
    .function("InitPopulation", &Population::InitPopulation)
    .function("Step_FullDevelopment", &Population::Step_FullDevelopment)
    .function("Step_FullDevelopmentN", &Population::Step_FullDevelopmentN)
    .function("GetFittestIdx", &Population::GetFittestIdx)
    .function("GetFitness", &Population::GetFitness)
    .function("GetFitnessWidthHeight", &Population::GetFitnessWidthHeight)
    .function("GetGenotype", &Population::GetGenotype)
    .function("GetFitnessDist", &Population::GetFitnessDist)
    .function("GetTreePopulation", &Population::GetTreePopulation)
    .function("GetHeight", &Population::GetHeight)
    .function("GetWidth", &Population::GetWidth)
    .function("GetEnvFitnesses", &Population::GetEnvFitnesses)
    .function("GetTree", &Population::GetTree)
    .function("GetLightGrid", &Population::GetLightGrid)
    .function("RecordAgent", &Population::RecordAgent)
    .function("GetRecordedLightGrids", &Population::GetRecordedLightGrids)
    .function("GetRecordedCellGrids", &Population::GetRecordedCellGrids)
    .function("SetConfigurableParameters", &Population::SetConfigurableParameters)
    ;

  class_<Agent>("Agent")
    .constructor()
    .function("Develop", &Agent::Develop)
    ;
}
