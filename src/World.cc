#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <fstream>
#include <cstring>
#include <math.h>
#include <chrono>
#include "Agent.hh"
#include "Population.hh"
#include "random.hh"

// vector <unique_ptr<Agent>> trees;
// vector <unique_ptr<Agent>> new_trees;
extern Parameter par;

void Start(int argc, char *argv[])
{
    /*do stuff like making a directory or setting up stuff for 
    interfacing with webassemby
    */

    //call parameter reading
    if(argc>2){
        cerr<<"Reading paramers"<<endl;
        par.nutrient_influx = stof(argv[1]);
        cerr<<argv[1]<<endl;
        par.energy_synthesis = stof(argv[2]);
        cerr<<argv[2]<<endl;
        par.threshold_energy = stof(argv[3]);
        cerr<<argv[3]<<endl;
        par.threshold_nutrients = stof(argv[4]);
        cerr<<argv[4]<<endl;
        par.metabolic_rate = stof(argv[5]);
        cerr<<argv[5]<<endl;
        // par.ccs = stoi(argv[6]);
        // par.random_seeding = stoi(argv[7]);
        par.mutate_mu = stod(argv[8]);
        cerr<<argv[8]<<endl;
        par.mutate_mustd = stod(argv[9]);
        cerr<<argv[9]<<endl;
    }
}

int main(int argc, char *argv[]){
    int seed = static_cast<long unsigned int>(std::chrono::high_resolution_clock::now().time_since_epoch().count());
    Start(argc, argv);
    Seed(seed);
    srand(seed);
    Population *pop;
    string base = "results/";
    string name;
    auto start = std::chrono::system_clock::now();
    pop = new Population();
    pop->InitPopulation();
    // cerr<<"Pop initialized"<<endl;
    
    for(int t=0; t<150;t++){
        pop->Step_FullDevelopment(t);
        // cerr<<"time "<<t<<endl;
        if(t%20==0){
            int bestkey = -1;
            float bestfitness = 0;
            for(auto& [key,ag] : pop->Trees){
                if(ag->fitness>bestfitness){
                    bestkey = key;
                    bestfitness = ag->fitness;
                }
            }
            // cerr<<"printing..."<<endl;
            // cerr<<pop->Trees[bestkey]->print_AgentGraph_dot()<<endl;
            // cerr<<pop->Trees[bestkey]->print_AgentGraph_edges()<<endl;
            
            /* pop->Trees[bestkey]->env->print_in_terminal("asciitree"); */
            /* pop->Trees[bestkey]->env->print_in_terminal("cellgrid"); */
            /* pop->Trees[bestkey]->env->print_in_terminal("lightgrid"); */
            // pop->Trees[bestkey]->print_in_terminal("nutrients");
            // pop->Trees[bestkey]->print_in_terminal("energy");
            // cerr<<"Time: "<<t<<endl;
        }
    }
    int bestkey = -1;
    float bestfitness = 0;
    for(auto& [key,ag] : pop->Trees){
        if(ag->fitness>bestfitness){
            bestkey = key;
            bestfitness = ag->fitness;
        }
    }
    
    /* pop->Trees[bestkey]->genome.PrintGenome("winner.txt"); */
    delete pop;
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout<<"elapsed time: " << elapsed_seconds.count() << std::endl;
}
