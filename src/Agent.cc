#include "stdio.h"
#include <stdlib.h> 
#include <algorithm> 
#include "Agent.hh"
#include "parameters.hh"
#include "Genome.hh"
#include "Environment.hh"

extern Parameter par;

Agent::Agent(){
    fitness=1.f;
}

Agent::~Agent(){
    cells.clear();
}

void Agent::set_environment(Environment<Agent> *environment){
    env = environment;
}

void Agent::Plant_seed(int x, int y){
    int cellid = get_new_cellid();
    cells.emplace_back(make_unique<Cell>(cellid, x, y, genome));
    cells.back()->nutrients[0] = par.metabolic_rate*5;
    cells.back()->nutrients[1] = par.metabolic_rate*5;
    env->cellgrid[y][x] = cellid;
}

void Agent::InitAgent(int aid){
    //init a genome
    agentid = aid;
    if(par.neumann_neighbourhood){
        if(!par.ccs){
            genome.InitGenome(2, 5, 3);
        }else{
            genome.InitGenome(3, 5, 4);
        }
    }else{
        if(!par.ccs){
            genome.InitGenome(2, 5, 2);
        }else{
            genome.InitGenome(3, 5, 3);
        }
    }
    
    // //from here becomes obsolete
    // //make our initial cell in the middle of the bottom layer
    // cells.emplace_back(make_unique<Cell>(1, par.width/2, 0, genome));
    // cells.back()->growtype = 0;
    // cells.back()->nutrients[0] = par.metabolic_rate*5;
    // cells.back()->nutrients[1] = par.metabolic_rate*5;
    // for(int i=0; i<par.height; i++){
    //     for(int j=0; j<par.width; j++){
    //         cellgrid[i][j]=0;
    //     }
    // }
    // cellgrid[0][par.width/2] = 1;

    // //make initial light grid
    // float lightval;
    // for(int i=0; i<par.height; i++){
    //     lightval = par.light_A*i/par.height + par.light_B;
    //     for(int j=0; j<par.width; j++){
    //         lightgrid[i][j] = lightval;
    //     }
    // }
    // update_lightgradient(par.width/2, 0);
}

void Agent::MakeAgentFromParent(Agent &A, int agentid)
{
    InitAgent(agentid);
    genome=A.genome;
    genome.MutateGenome(par.mutate_mu, par.mutate_mustd);
}

void Agent::Develop(int steps){
    //1: update cell states for all cells
    //2: update nutrient state all cells (influx + diffusion)
    //3: update energy state all cells   (f(light)+diffusion)
    int step;
    string option;
    for(step=0;step<steps;step++){
        option = "cellgrid";
        // print_in_terminal(option);
        option = "lightgrid";
        // print_in_terminal(option);
        option = "nutrients";
        // print_in_terminal(option);
        option = "energy";
        // print_in_terminal(option);
        //update cell states
        update_intracellstate();

        //update nutrient states and energy states
        update_nutrients();     //get nutrients & energy
        diffuse_nutrients();    //diffuse nutrients & energy

        //celldeath
        // update_celldeath();

        //check if cells can divide
        Divide_cells();
    }

    //fitness
    if (par.fitness == "size") {
      fitness = cells.size(); 
    }else if(par.fitness == "custom"){
      fitness = par.fbiomass * cells.size() + par.fwidth * GetWidth() + par.fheight * GetHeight();
    }else {
      fitness = GetHeight();
    }

    fitness = std::max((float)0.001, fitness);
    

}

int Agent::GetHeight() {
  /* if (myheight > 0){
    return myheight;
  } */
  if (cells.size() == 1) {
    myheight = 1;
    return 1;
  }

  int max_height = 0;
  for (const auto &cell : cells) {
    if (cell->coordinates[1] > max_height) {
      max_height = cell->coordinates[1];
    }
  }
  myheight = max_height + 1;
  return myheight;
}

int Agent::GetWidth() {
  /* if (mywidth > 0){
    return mywidth;
  } */
  if (cells.size() == 1) {
    mywidth = 1;
    return 1;
  }

  int min_width = 1e6;
  int max_width = 0;
  for (const auto &cell : cells) {
    if (cell->coordinates[0] > max_width) {
      max_width = cell->coordinates[0];
    }
    if (cell->coordinates[0] < min_width) {
      min_width = cell->coordinates[0];
    }
  }
  mywidth = max_width - min_width + 1;
  return mywidth;
}

void Agent::DevelopAndRecord(int steps){
    //1: update cell states for all cells
    //2: update nutrient state all cells (influx + diffusion)
    //3: update energy state all cells   (f(light)+diffusion)
    int step;
    string option;

    recorded_lightgrids.emplace_back(lightgrid);
    recorded_cellgrids.emplace_back(cellgrid);

    for(step=0;step<steps;step++){
        option = "cellgrid";
        // print_in_terminal(option);
        option = "lightgrid";
        // print_in_terminal(option);
        //update cell states
        // cerr<<step<<endl;
        update_intracellstate();

        //update nutrient states and energy states
        update_nutrients();     //get nutrients & energy
        diffuse_nutrients();    //diffuse nutrients & energy

        //check if cells can divide
        Divide_cells();
        recorded_lightgrids.emplace_back(lightgrid);
        recorded_cellgrids.emplace_back(cellgrid);
      
    }
    fitness = cells.size();
     
    // float val;
    // for(int i=par.height-1; i>=0; i--){
    //     for(int j=0; j<par.width; j++){
    //         val = cellgrid[i][j];
    //         if(val!=0){
    //             fitness=i+1;
    //             return;
    //         }
    //     }
    // }
}

//function that makes a string out of the cell position for graphviz. Adds 0.5 to put the node in the centre
std::string Agent::coordstring(const Cell &c)
{
    return to_string(c.coordinates[0]+0.5)+","+to_string(c.coordinates[1]+0.5);
}

std::string Agent::print_AgentGraph_dot(void)
{
    double thickness=2.0;
    std::string graph;

    graph="digraph G { \n";
    graph+="layout=\"neato\"\n";
    graph+="bgcolor=\"#ffffff00\";\n";

    graph+="node [fontname=\"Open Sans\",fontsize=12, style=filled, shape=\"point\", color=\"#f0f0f0\", pin=true];\n";

    for (auto &c: cells){
        graph+= to_string(c->cellid)+" [ pos = \""+coordstring(*c)+"\"]\n";
        if (c->parentindex!=-1){
            graph+=to_string(cells[c->parentindex]->cellid)+" -> "+to_string(c->cellid)+"[color=\"#f0f0f0\"]\n";
        }
    }
    graph+="}\n\n\n";

    return graph;
    
}

//list of edges, comma separated: x1,y1,x2,y2
std::string Agent::print_AgentGraph_edges(void)
{
    std::string edges="";

    for (auto &c: cells){
        if (c->parentindex!=-1){
            edges+=coordstring(*cells[c->parentindex])+","+coordstring(*c)+"\n"; //each cell records the edge with its parent
        }
        
    }

    return edges;
}

void Agent::UpdateNeighbours(Cell *cell){
    //only add neighbour of same plant
    int x = cell->coordinates[0];
    int y = cell->coordinates[1];
    Cell *nb;
    int cellid;
    // (val-1)/par.width*par.height;
    int wh = par.width*par.height;

    cellid = abs(env->cellgrid[y+1][x]);
    if(cellid!=0 && (cellid-1)/wh==agentnr){
        nb = cells[(cellid-1)%wh].get();
        cell->neighbours.push_back(nb);
        nb->neighbours.push_back(cell);
    }

    if(y>0){
        cellid = abs(env->cellgrid[y-1][x]);
        if(cellid!=0 && (cellid-1)/wh==agentnr){
            nb = cells[(cellid-1)%wh].get();
            cell->neighbours.push_back(nb);
            nb->neighbours.push_back(cell);
        }
    }

    cellid = abs(env->cellgrid[y][x+1]);
    if(cellid!=0 && (cellid-1)/wh==agentnr){
        nb = cells[(cellid-1)%wh].get();
        cell->neighbours.push_back(nb);
        nb->neighbours.push_back(cell);
    }

    cellid = abs(env->cellgrid[y][x-1]);
    if(cellid!=0 && (cellid-1)/wh==agentnr){
        nb = cells[(cellid-1)%wh].get();
        cell->neighbours.push_back(nb);
        nb->neighbours.push_back(cell);
    }
    
    //in the case of moore nb add the diagonal neighbours
    if(!par.neumann_neighbourhood){
        cellid = abs(env->cellgrid[y+1][x+1]);
        if(cellid!=0 && (cellid-1)/wh==agentnr){
            nb = cells[(cellid-1)%wh].get();
            cell->neighbours.push_back(nb);
            nb->neighbours.push_back(cell);
        }

        if(y>0){
            cellid = abs(env->cellgrid[y-1][x-1]);
            if(cellid!=0 && (cellid-1)/wh==agentnr){
                nb = cells[(cellid-1)%wh].get();
                cell->neighbours.push_back(nb);
                nb->neighbours.push_back(cell);
            }

            cellid = abs(env->cellgrid[y-1][x+1]);
            if(cellid!=0 && (cellid-1)/wh==agentnr){
                nb = cells[(cellid-1)%wh].get();
                cell->neighbours.push_back(nb);
                nb->neighbours.push_back(cell);
            }
        }


        cellid = abs(env->cellgrid[y+1][x-1]);
        if(cellid!=0 && (cellid-1)/wh==agentnr){
            nb = cells[(cellid-1)%wh].get();
            cell->neighbours.push_back(nb);
            nb->neighbours.push_back(cell);
        }
    }
}

void Agent::Divide_cells(){
    int new_id;
    int x;
    int y;
    int xnew;
    int ynew;
    int size;
    int grow;
    vector<int> outnodes;
    outnodes.reserve(3);//reserve three spots, moore uses 2 neumann 3
    size = cells.size();
    for(int i=0;i<size;i++){
        //if cell not alive it cant reproduce
        if(!cells[i]->alive){
            continue;
        }

        //if not enough nutrients it cant reproduce
        if(!(cells[i]->nutrients[0]>par.threshold_nutrients && 
             cells[i]->nutrients[1]>par.threshold_energy)){
            continue;
        }
        if(par.neumann_neighbourhood){
            outnodes[0] = cells[i]->outnodes[0];
            outnodes[1] = cells[i]->outnodes[1];
            outnodes[2] = cells[i]->outnodes[2];
            
            //check if outnode is on reproduce
            if(!(outnodes[0])){
                continue;
            }
        }else{
            outnodes[0] = cells[i]->outnodes[0];
            outnodes[1] = cells[i]->outnodes[1];

            //check if outnode is on reproduce
            if(abs(outnodes[0])+abs(outnodes[1])==0){
                continue;
            }
        }

        x = cells[i]->coordinates[0];
        y = cells[i]->coordinates[1];
        
        if(par.neumann_neighbourhood){
             //true true > up
            if(cells[i]->outnodes[1]&&cells[i]->outnodes[2]){
                xnew = x;
                ynew = y+1;
                grow=0;
            }
            //false false > down
            if(!cells[i]->outnodes[1]&&!cells[i]->outnodes[2]){
                xnew = x;
                ynew = y-1;
                grow=0;
            }
            //true false > left
            if(cells[i]->outnodes[1]&&!cells[i]->outnodes[2]){
                xnew = x-1;
                ynew = y;
                grow=1;
            }
            //false true > right
            if(!cells[i]->outnodes[1]&&cells[i]->outnodes[2]){
                xnew = x+1;
                ynew = y;
                grow=2;
            }
        }else{
            xnew = x + cells[i]->outnodes[0];
            ynew = y + cells[i]->outnodes[1];
            //if both outputs are 0, you do not divide (taken care of above)
        }
       
        //check grid EXAMPLE
        if(ynew >= 0 && ynew < par.height-1 && xnew > 0 && xnew < par.width-1){
            if(env->cellgrid[ynew][xnew] == 0){
                //new cell
                new_id = get_new_cellid();
                env->cellgrid[ynew][xnew] = new_id;
                cells.emplace_back(make_unique<Cell>(new_id, xnew, ynew, genome));
                cells.back()->Cellbirth(*cells[i], i); //copy states from parent
                cells.back()->growtype = grow;
                //do something with parent states
                if(par.nb_old){
                    UpdateNeighbours(cells.back().get());
                }else{
                    cells.back()->neighbours.push_back(cells[i].get());
                    cells[i].get()->neighbours.push_back(cells.back().get());
                }
                
                //update lightgrid
                env->update_lightgradient(xnew, ynew);
                cells[i]->nutrients[0]*=0.5;
                cells[i]->nutrients[1]*=0.5;
            };
        }
    }
}
int Agent::get_new_cellid(){
    return par.width*par.height*agentnr+cells.size()+1;
}

void Agent::update_intracellstate()
{
    for (auto &c: cells){
        if(c->alive){
            c->update_cellstate(genome);
            if(par.celldeath){
                c->nutrients[0] -= par.metabolic_rate;
                c->nutrients[1] -= par.metabolic_rate;
                if(c->nutrients[0]<0||c->nutrients[1]<0){
                    c->alive = false;
                    env->cellgrid[c->coordinates[1]][c->coordinates[0]] = -1*c->cellid;
                }
            }
        }
    }
}

void Agent::update_nutrients()
{
    int x;
    int y;
    for(auto &cell: cells){
        if(!cell->alive){
            continue;
        }
        x = cell->coordinates[0];
        y = cell->coordinates[1];
        if(y==0){
            cell->nutrients[0] += par.nutrient_influx; //nutrients
        }
        cell->nutrients[1] += env->lightgrid[y][x]*0.1*par.energy_synthesis;     //energy
    }
}

void Agent::diffuse_nutrients()
{
    int nr_nb;
    float totnutrients;
    float totenergy;
    if(par.direct_diffusion){
        totnutrients = 0;
        totenergy = 0;

        for(auto &cell: cells){
            totnutrients += cell->nutrients[0];
            totenergy += cell->nutrients[1];
        }
        totnutrients/=cells.size();
        totenergy/=cells.size();

        for(auto &cell: cells){
            cell->nutrients[0] = totnutrients;
            cell->nutrients[1] = totenergy;
        }
        return;
    }
    //compute diffusion nutrient states and energy states
    for(auto &cell: cells){
        cell->diffstates[0] = 0;
        cell->diffstates[1] = 0;
        nr_nb=0;
        for(auto &nb: cell->neighbours){
            cell->diffstates[0] += nb->nutrients[0];
            cell->diffstates[1] += nb->nutrients[1];
            nr_nb++;
        }
        cell->diffstates[0]-= nr_nb*cell->nutrients[0];
        cell->diffstates[1]-= nr_nb*cell->nutrients[1];
    }
    //finish diffusion nutrient and energy states
    for(auto &cell: cells){
        cell->nutrients[0] += cell->diffstates[0]*par.D_nutrients;
        cell->nutrients[1] += cell->diffstates[1]*par.D_energy;
    }
}

void Agent::update_lightgradient(int x, int y){
    //Update the lightgradient around the given coordinates
    // lightgrid[y][x] *= 0.4;
    lightgrid[y][x-1]*= par.side_shade;
    lightgrid[y][x+1] *= par.side_shade;
    if(y>0){
        lightgrid[y-1][x] *= par.under_shade;
        lightgrid[y-1][x-1]*= par.under_shade;
        lightgrid[y-1][x+1] *= par.under_shade;
    }
}

// void Agent::SaveData(string file){


//     std::ofstream ofs;
//     ofs.open( filename , std::ofstream::out);
    
//     ofs << "[";

//     ofs << Genome.innr<<", "<<regnr<<", "<<outnr;   

//     for (auto &n: inputscale){
//         ofs<< ", "<< n;
//     }

//     for (auto&n: thresholds){
//         ofs <<", "<< n;
//     }

//     for (auto &n: reglayer_input){
//         for(auto &p: n){
//             ofs <<", "<< p;
//         }
//     }

//     for (auto &n: outlayer_input){
//         for(auto &p: n){
//             ofs <<", "<< p;
//         }         
//             // i++;
//     }

//     ofs << "]"<<endl;

//     ofs.flush();
//     ofs.close();
//     return;
// }

// void Agent::print_in_terminal(string option){
//     //mind that we need to print top to bottom
//     float val;
//     int idx;
//     char skip[] = "   ";
//     char lr[] = "   ";
//     char l[] = " \\ ";
//     char r[] = " / ";
//     char ud[] = " | ";
//     printf("\n");
//     if(option=="cellgrid"){
//         for(int i=par.height-1; i>=0; i--){
//             for(int j=0; j<par.width; j++){
//                 val = env->cellgrid[i][j];
//                 if(val==0){
//                     printf("%3s ", skip);
//                 }else{
//                     printf("%3.f ", val);
//                 }
//             }
//             printf("\n");
//         }
//     }
//     if(option=="lightgrid"){
//         for(int i=par.height-1; i>=0; i--){
//             for(int j=0; j<par.width; j++){
//                 val = env->lightgrid[i][j];
//                     // printf("%i", nr);
//                 printf("%3.f ", val);
//             }
//             printf("\n");
//         }
//     }
//     if(option=="asciitree"){
//         for(int i=par.height-1; i>=0; i--){
//             for(int j=0; j<par.width; j++){
//                 val = env->cellgrid[i][j];
//                 if(val==0){
//                     printf("%s", "_");
//                 }else{
//                     // // printf("%3.f", val);
//                     // if(cells[int(val)-1]->growtype==0){
//                     //     printf("%3s", ud);
//                     // }else if (cells[int(val)-1]->growtype==1)
//                     // {
//                     //     printf("%3s", l);
//                     // }else
//                     // {printf("%3s", r);}

//                     // printf("%s", cells[val]->growtype);
//                     printf("%s", "#");
//                 }
//             }
//             printf("\n");
//         }
//     }
//     if(option=="nutrients"){
//         for(int i=par.height-1; i>=0; i--){
//             for(int j=0; j<par.width; j++){
//                 idx = abs(env->cellgrid[i][j]);
//                 if(idx>0){
//                     val = cells[idx-1]->nutrients[0];
//                     printf("%3.f ", val);
//                 }else{
//                     printf("%3s ", skip);
//                 }

//             }
//             printf("\n");
//         }
//     }
//     if(option=="energy"){
//         for(int i=par.height-1; i>=0; i--){
//             for(int j=0; j<par.width; j++){
//                 idx = abs(env->cellgrid[i][j]);
//                 if(idx>0){
//                     val = cells[idx-1]->nutrients[1];
//                     printf("%3.f ", val);
//                 }else{
//                     printf("%3s ", skip);
//                 }
//             }
//             printf("\n");
//         }
//     }
// }
