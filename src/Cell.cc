#include "stdio.h"
#include <stdlib.h> 
#include <cmath> 
#include "Cell.hh"
#include "Genome.hh"
#include "parameters.hh"

extern const Parameter par;

Cell::~Cell(){
}

Cell::Cell(int id, int x, int y, Genome &G)
{
    cellid=id;
    coordinates[0]=x;
    coordinates[1]=y;

    nutrients[0]=0.;
    nutrients[1]=0.;
    nutrients[2]=0.;
    parentindex=-1;

    //set input states
    sensory = std::vector<double>(G.innr, 0.0);
    regnodes = std::vector<bool>(G.regnr, false);
    outnodes = std::vector<int>(G.outnr, 0);
}

void Cell::Cellbirth(const Cell &Parent, int index)
{
    //id and coordinates should be set elsewhere
    nutrients[0]=Parent.nutrients[0]*0.5;
    nutrients[1]=Parent.nutrients[1]*0.5;
    nutrients[2]=Parent.nutrients[2];
    //set input states
    sensory=Parent.sensory;
    regnodes=Parent.regnodes;
    outnodes=Parent.outnodes;
    parentindex=index;
}

double Cell::getNeighInput(){
    double signal=0.;
    int ccsnode = 3;
    if(par.neumann_neighbourhood){
        ccsnode=3;
    }else{
        ccsnode=2;
    }
    for (auto &n: neighbours){
        signal+=n->outnodes[ccsnode];
    }
    return signal;
}

void Cell::update_cellstate(const Genome &G)
{
    double newval;
    vector <bool> newreg; //to keep the new states for the regulatory layer while updating this layer (they can cross regulate, use old state for that)
    newreg.reserve(G.regnr);
    int i;
    int j;
    if(par.ccs){
        nutrients[2]=getNeighInput();
    }
    //filter input data with evolved weights
    for (i=0; i<G.innr; i++){
        sensory[i]=G.inputscale[i]*nutrients[i];
    }

    //update regulatory nodes
    for (i=0; i<G.regnr; i++){
        newval=0.;
        
        //inputs from sensory layer
        for (j=0; j<G.innr; j++){
            newval+=std::tanh(sensory[j]*G.reglayer_input[i][j]);
        }
        //inputs from regulatory layer
        for (j=0; j<G.regnr; j++){
            newval+=std::tanh(regnodes[j]*G.reglayer_input[i][j+G.innr]);
        }
        if(newval>G.thresholds[i]){
            newreg.push_back(1);
        }else{
            newreg.push_back(0);
        }
    }
    //now update the nodes
    regnodes=newreg;

    //update output nodes (which are ints!)
    for (i=0; i<G.outnr; i++){
        newval=0.;
        //inputs from regulatory layer
        for (j=0; j<G.regnr; j++){
            newval+=regnodes[j]*G.outlayer_input[i][j];
        }

        if (par.neumann_neighbourhood){
            if(newval>G.thresholds[i+G.regnr]){
                outnodes[i]=1;
            }else{
                outnodes[i]=0;
            }
        }else{
            if(newval<(-G.thresholds[i+G.regnr])){
                outnodes[i]=-1;
            }else if(newval>G.thresholds[i+G.regnr]){
                outnodes[i]=1;
            }else{
                outnodes[i]=0;
            }   
        }
    }
}
