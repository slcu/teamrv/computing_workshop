#include "Genome.hh"
#include "random.hh"
#include "parameters.hh"

extern Parameter par;

//default constructor required for init in other class
Genome::Genome()
{
  innr=0;
  regnr=0;
  outnr=0;
}

Genome::Genome(int in, int reg, int out)
{
  InitGenome(in, reg, out);
}

void Genome::InitGenome(int in, int reg, int out)
{
  int i,j;

  innr=in;
  regnr=reg;
  outnr=out;

  //how to scale the input
  for(i=0; i<innr; i++){
    inputscale.push_back(0.1);
  }

  for (i=0; i<reg; i++){
    reglayer_input.push_back(vector<double>());
    thresholds.push_back(0.);
    for (j=0; j<in+reg; j++){
        reglayer_input[i].push_back(0.0); 
    }
  }

  for (i=0; i<out; i++){
    outlayer_input.push_back(vector<double>());
    thresholds.push_back(0.);
    for (j=0; j<reg; j++){
        outlayer_input[i].push_back(0.);
    }
  }

}

Genome::~Genome()
{
 //cout<<"destructed"<<endl;
}
//copy constructor
Genome::Genome(const Genome &Parent)
{
  innr=Parent.innr;
  regnr=Parent.regnr;
  outnr=Parent.outnr;

  inputscale=Parent.inputscale;
  //inputnodes=Parent.inputnodes;
  reglayer_input=Parent.reglayer_input;
  outlayer_input=Parent.outlayer_input;
  thresholds=Parent.thresholds;
}

void Genome::MutateGenome(double mu, double mustd)
{
    //int i,j;

    for(auto &n: inputscale){
        if(uniform()<mu){
            n+=returnNormal(0., mustd);
        }
    }

    for (auto &n: reglayer_input){
        for(auto &p: n){
            if(uniform()<mu){ //if this parameter mutates
                p+=returnNormal(0., mustd); //then pull a normally distributed mutation
            }
        }
    }

    for (auto &n: outlayer_input){
        for(auto &p: n){
            if(uniform()<mu){
                p+=returnNormal(0., mustd);
            }
        }
    }

    for (auto &n: thresholds){
        if(uniform()<mu){
            n+=returnNormal(0., mustd);
        
        }

    }
}

/* void Genome::PrintGenome(std::string filename)
{

    std::ofstream ofs;
    ofs.open( filename , std::ofstream::out);
    
    ofs << "[";

    ofs << innr<<", "<<regnr<<", "<<outnr;   

    for (auto &n: inputscale){
        ofs<< ", "<< n;
    }

    for (auto&n: thresholds){
        ofs <<", "<< n;
    }

    for (auto &n: reglayer_input){
        for(auto &p: n){
            ofs <<", "<< p;
        }
    }

    for (auto &n: outlayer_input){
        for(auto &p: n){
            ofs <<", "<< p;
        }         
            // i++;
    }

    ofs << "]"<<endl;

    ofs.flush();
    ofs.close();
} */

string Genome::PrintGenome()
{
    double thickness=2.0;
    double maxthick=10.0;
    string graph;

    graph="digraph G { \n";
    graph+="graph [pad=\"0.1\", nodesep=\"0.2\", ranksep=\"0.65\", margin=\"0\"];layout=\"dot\"\nrankdir=LR\nsplines=line\n";

    graph+="bgcolor=\"#F5F5F4\";\n";

    graph+="node [fontname=\"Open Sans\",fontsize=12,style=filled];\n";

    vector <string> inlabels ={"nutrients", "light"}; //change order if necessary!
    vector <string> outlabels ={"division", "direction 1"}; //change order if necessary!
    if(par.ccs){
        inlabels.push_back("signals");
    }
    if(par.neumann_neighbourhood){
        outlabels.push_back("direction 2");    
    }
    if(par.ccs){
        outlabels.push_back("signals");
    }
    
    // subgraphs
    graph += "subgraph input {\ncolor=white;\nnode [label=\"\",style=solid,color=blue4, shape=circle, width=0.7];\n rank = same;";
    for (int i=0; i<innr; i++){
        /* graph+=to_string(i) + " ";   */
        graph+=to_string(i)+"[label=\""+inlabels[i]+"\", penwidth=3] ";  
    }
    graph += ";\n}";

    graph += "subgraph reg {\ncolor=white;\nnode [label=\"\",style=solid,color=red2, shape=circle, width=0.7];\n rank = same;";
    for (int i=innr; i<innr+regnr; i++){
        graph+=to_string(i) + "[penwidth=1.5] ";

    }
    graph += ";\n}";

    graph += "subgraph out {\ncolor=white;\nnode [label=\"\",style=solid,color=green2, shape=circle, width=0.7];\n rank = same;";
    int k = 0;
    for (int i=innr+regnr; i<innr+regnr+outnr; i++){
        graph+=to_string(i)+"[label=\""+outlabels[k]+"\", penwidth=3] ";  
        k++;
    }
    graph += ";\n}";
    int count=0;
    for (int i=0; i<innr; i++){
        /* graph+=to_string(count)+" [label=\""+inlabels[i]+"];\n";  */
        count++;
    }
    for (int i=0; i<regnr; i++){
        /* graph+=to_string(count)+" [label=\""+to_string(i)+", "+to_string(thresholds[i])+"\" color=\"salmon\"];\n"; */
        
        for (int j=0; j<innr+regnr; j++ ) {
            if (reglayer_input[i][j]<-0.0001){
                graph+=to_string(j)+"-> "+to_string(count)+"[color=\"#c92f2490\", arrowsize=\"0.7\", penwidth="+to_string(max(0.4, min(maxthick,thickness*abs(reglayer_input[i][j]))))+"];\n";
            }else{
                graph+=to_string(j)+"-> "+to_string(count)+"[color=\"#2121c490\" arrowsize=\"0.7\", penwidth="+to_string(max(0.4, min(maxthick,thickness*abs(reglayer_input[i][j]))))+"];\n";
            }
        
        }
        count++;
    }
    
    for (int i=0; i<outnr; i++){
        for (int j=0; j<regnr; j++ ) {
            if (outlayer_input[i][j]<-0.0001){
                graph+=to_string(innr+j)+"-> "+to_string(count)+"[color=\"#c92f2490\" arrowsize=\"0.7\", penwidth="+to_string(max(0.4, min(maxthick,thickness*abs(outlayer_input[i][j]))))+"];\n";
            }else{
                graph+=to_string(innr+j)+"-> "+to_string(count)+"[color=\"#2121c490\" arrowsize=\"0.7\", penwidth="+to_string(max(0.4, min(maxthick,thickness*abs(outlayer_input[i][j]))))+"];\n";
            }
        
        }
        count++;
    }
    graph+="}\n\n\n";

    return graph;
}
