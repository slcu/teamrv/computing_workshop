#include "Population.hh"
#include <iostream>
#include <algorithm>
#include <fstream>
#include <string>
#include "random.hh"
#include "Agent.hh"
#include "parameters.hh"
#include "Environment.hh"
#include <chrono>

extern Parameter par;

Population::~Population(){
  for(auto& [key,ag] : Trees){
      // cerr<<"Deleting ag with key: "<<key<<endl;
      delete ag; // inside here it breaks 
    }
    Trees.clear();
  for (auto& env : environments){
    delete env;
  }
  environments.clear();
}

void Population::InitPopulation(){
  int seed = static_cast<long unsigned int>(std::chrono::high_resolution_clock::now().time_since_epoch().count());
  Seed(seed);
  srand(seed);

  bool environment=true;
  bool planted;
    for(int i=0; i<par.popsize; i++){
        Trees[i] = new Agent;
        Trees[i]->InitAgent(i);
        // Trees[i]->Develop(par.steps);
        cerr<<i<<endl;
        // Trees[i]->print_in_terminal("lightgrid");
        // Trees[i]->print_in_terminal("cellgrid");
        current_largest_id=i;
        // Trees.emplace_back(make_unique<Agent>());
        // Trees.back()->InitAgent();
        // Trees.back()->Develop(par.steps);
        // Trees.back()->print_in_terminal("lightgrid");
        // Trees.back()->print_in_terminal("cellgrid");
        // cerr<<"TEST"<<endl;
        /* if (!i){
          cerr<<Trees[i]->genome.PrintGenome()<<endl;
        } */
    }
    if(environment){
     //Environment<Agent> *env;
      int seedspot;
      for(int k=0; k<par.popsize; k+=par.nr_of_trees){
        /* cerr<<k<<endl; */
        //env = new Environment<Agent>;
        //environments.push_back(env);
        environments.push_back(new Environment<Agent>);
        for(int j=0; j<par.nr_of_trees; j++){
          if(par.random_seeding){
            planted = false;
            while(!planted){
              seedspot = uniform()*par.width;
              planted = environments.back()->plant_tree(Trees[k+j], seedspot, 0);
            }  
          }else{
            seedspot = par.width/(par.nr_of_trees+2)*(j+1);
            environments.back()->plant_tree(Trees[k+j], seedspot, 0);
          }
        }

        /* cerr<<"planted"<<endl; */
        environments.back()->Develop(par.steps);
        /* environments.back()->print_in_terminal("asciitree"); */
        // environments[0]->print_in_terminal("lightgrid");
        
      }
    }
}

void Population::Step_FullDevelopmentN(int Time, int N){
  for (int i = 0; i < N; i++) {
    Step_FullDevelopment(Time + i);
  }
}

//Develops Organisms entirely
void Population::Step_FullDevelopment(int Time){
  double total_fitness=0.;
  std::vector<int> keys;
  int seedspot;
  bool planted;
  std::vector<pair<int,double>> v_key_fitness; // vector of keys of those that reproduce
  v_key_fitness.reserve(par.popsize);
  
  for( const auto& [key, ag] : Trees){
    v_key_fitness.push_back(make_pair(key,ag->fitness));
    //cerr<<"Fitness: "<<ag->fitness<<endl;
    total_fitness += ag->fitness;
  }
  newTrees.clear();

  if(total_fitness > 0.){
    for(int i=0 ; i<par.popsize; i++){
      double rn = total_fitness*uniform();
      double cum_fitness = 0.;
      for(const auto &[key,fitness]: v_key_fitness){
        cum_fitness += fitness;
        if(cum_fitness > rn){
          newTrees[i] = new Agent();
          newTrees[i]->MakeAgentFromParent(*Trees[key], i);
          break;
        }
      }
    }

    //clear Ags to receive the new population
    for(auto& [key,ag] : Trees){
      // cerr<<"Deleting ag with key: "<<key<<endl;
      delete ag; // inside here it breaks 
    }
    Trees.clear();

    //now put the new population in Ags
    for(const auto &[key2,ag2]: newTrees ){
      int new_id = NewId();
      Trees[new_id] = ag2; 
      Trees[new_id]->agentid=new_id;
      keys.push_back(new_id);
      // Trees[new_id]->Develop(par.steps);
    }
    for(int k=0; k<par.popsize; k+=par.nr_of_trees){
        // cerr<<k<<endl;
        
        environments[k/par.nr_of_trees]->reset();
        for(int j=0; j<par.nr_of_trees; j++){
          if(par.random_seeding){
            planted = false;
            while(!planted){
              seedspot = uniform()*par.width;
              planted = environments[k/par.nr_of_trees]->plant_tree(Trees[keys[k+j]], seedspot, 0);
            }  
          }else{
            seedspot = par.width/(par.nr_of_trees+2)*(j+1);
            environments[k/par.nr_of_trees]->plant_tree(Trees[keys[k+j]], seedspot, 0);
          }
        }
        environments[k/par.nr_of_trees]->Develop(par.steps);
        
        // environments[k/3]->plant_tree(Trees[keys[k]], 5, 0);
        // environments[k/3]->plant_tree(Trees[keys[k+1]], 10, 0);
        // environments[k/3]->plant_tree(Trees[keys[k+2]], 15, 0);
        
        // environments[k]->print_in_terminal("cellgrid");
        // environments[0]->print_in_terminal("lightgrid");
        // environments[0]->reset();
    }
  }
}

int Population::GetFittestIdx() {
  int bestkey = -1;
  float bestfitness = 0;
  for(auto& [key,ag] : Trees){
    if(ag->fitness>bestfitness){
      bestkey = key;
      bestfitness = ag->fitness;
    }
  }

  return bestkey;
}

std::vector<float> Population::GetEnvFitnesses(int agent_idx) {
  std::vector<float> fitnesses;

  for (const auto &agent : Trees[agent_idx]->env->agents) {
    fitnesses.push_back(agent->fitness);
  }

  return fitnesses;
}

float Population::GetFitness(int idx) {
  return Trees[idx]->fitness;
}

std::vector<float> Population::GetFitnessWidthHeight(int idx) {
  return {GetFitness(idx), (float)GetWidth(idx), (float)GetHeight(idx)};
}

std::string Population::GetGenotype(int idx) {
  return Trees[idx]->genome.PrintGenome();
}

std::vector<float> Population::GetFitnessDist() {
  std::vector<float> fitness_dist;

  for (const auto &[_, tree] : Trees) {
    fitness_dist.push_back(tree->fitness);
  }

  return fitness_dist;
}

int Population::GetHeight(int idx) {
  return Trees[idx]->GetHeight();
}

int Population::GetWidth(int idx) {
  return Trees[idx]->GetWidth();
}

std::vector<std::vector<int>> Population::GetTree(int idx) {
  return Trees[idx]->env->cellgrid;
}

std::vector<PopulationData> Population::GetTreePopulation() {
  std::vector<PopulationData> population;

  for (const auto &env : environments) {
    std::vector<float> fitnesses;
    for (const auto &agent : env->agents) {
      fitnesses.push_back(agent->fitness);
    }

    population.push_back(PopulationData(env->cellgrid, env->lightgrid, fitnesses));
  }

  return population;
}

std::vector<std::vector<float>> Population::GetLightGrid(int idx) {
  return Trees[idx]->env->lightgrid;
}

void Population::RecordAgent(int idx) {
  Trees[idx]->DevelopAndRecord(par.steps);
}

std::vector<std::vector<std::vector<float>>> Population::GetRecordedLightGrids(
    int idx) {
  return Trees[idx]->recorded_lightgrids;
}

std::vector<std::vector<std::vector<int>>> Population::GetRecordedCellGrids(
    int idx) {
  return Trees[idx]->recorded_cellgrids;
}


void Population::SetConfigurableParameters(float nutrient_influx,
    float energy_synthesis, float under_shade, int nr_trees, std::string fitness, 
    float fbiomass, float fwidth, float fheight, float mutation_rate) {
  par.nutrient_influx = nutrient_influx;
  par.energy_synthesis = energy_synthesis;
  par.under_shade = 1-under_shade;
  par.nr_of_trees = nr_trees;
  par.fbiomass = fbiomass;
  par.fwidth = fwidth;
  par.fheight = fheight;
  if (par.nr_of_trees == 1) {
    par.random_seeding = false;
  } else {
    par.random_seeding = true;
  }
  par.fitness = fitness;
  par.mutate_mu = mutation_rate;
}
