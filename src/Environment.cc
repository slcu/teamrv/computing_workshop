#include "stdio.h"
#include <stdlib.h>
#include "Environment.hh"
#include "Agent.hh"
#include "Genome.hh"

extern Parameter par;
template<typename T>
Environment<T>::Environment(){
    //fill the cellgrid
    cellgrid = vector<vector<int>>(par.height, vector<int>(par.width, 0));

    //fill the lightgrid
    if (!default_grid_initialized) {
      float lightval;
      for(int i=0; i<par.height; i++){
          lightval = par.light_A*i/par.height + par.light_B;
          for(int j=0; j<par.width; j++){
              DEFAULT_LIGHT_GRID[i][j] = lightval;
          }
      }
      default_grid_initialized = true;
    }

    lightgrid = DEFAULT_LIGHT_GRID;
}

template<typename T>
Environment<T>::~Environment(){
}

template<typename T>
void Environment<T>::update_lightgradient(int x, int y){
    //Update the lightgradient around the given coordinates
    // lightgrid[y][x] *= 0.4;
    if(x>0){
        lightgrid[y][x-1]*= par.side_shade;
    }
    if(x<par.width-1){
        lightgrid[y][x+1] *= par.side_shade;
    }
    
    if(y>0){
        lightgrid[y-1][x] *= par.under_shade; 
        lightgrid[y-1][x-1] *= par.under_shade;
        lightgrid[y-1][x+1] *= par.under_shade;
    }
    if(par.raytracing){
        for(int i=y-1; i>=0; i--){
          /* if (cellgrid[i][x] != 0) {
            // we hit another cell, don't cast shadow through cell
            break;
          } */
          lightgrid[i][x] *= 0.8*par.under_shade;
        }
    }
}

template<typename T>
bool Environment<T>::plant_tree(T *tree, int x, int y){
    if(cellgrid[y][x]==0){
        agents.push_back(tree); //this should be a reference to the tree in population
        agents.back()->agentnr=numtrees;
        tree->set_environment(this);
        tree->Plant_seed(x, y);
        update_lightgradient(x, y);
        numtrees = agents.size();
        return true;
    }else{
        // cerr<<"faka hier is al een boom joh";
        return false;
    }
}

template<typename T>
void Environment<T>::reset(){
    // for(auto tree: agents){
    //     delete tree;
    // }
    agents.clear();
    numtrees = 0;

    //fill the cellgrid
    cellgrid = vector<vector<int>>(par.width, vector<int>(par.height, 0));

    //fill the lightgrid
    lightgrid = DEFAULT_LIGHT_GRID;
}

template<typename T>
void Environment<T>::Develop(int steps){
    for(int i=0; i<steps; i++){
        for(int k=0; k<numtrees; k++){
            agents[(k+i)%numtrees]->Develop(1);//alternate the development
        }
    }
}

template<class T> 
void Environment<T>::print_in_terminal(std::string option){
    //mind that we need to print top to bottom
    float val;
    int idx;
    char skip[] = "   ";
    char lr[] = "   ";
    char l[] = " \\ ";
    char r[] = " / ";
    char ud[] = " | ";
    printf("\n");
    if(option=="cellgrid"){
        for(int i=par.height-1; i>=0; i--){
            for(int j=0; j<par.width; j++){
                val = cellgrid[i][j];
                if(val==0){
                    printf("%3s ", skip);
                }else{
                    printf("%3.f ", val);
                }
            }
            printf("\n");
        }
    }
    if(option=="lightgrid"){
        for(int i=par.height-1; i>=0; i--){
            for(int j=0; j<par.width; j++){
                val = lightgrid[i][j];
                    // printf("%i", nr);
                printf("%3.f ", val);
            }
            printf("\n");
        }
    }
    if(option=="asciitree"){
        for(int i=par.height-1; i>=0; i--){
            for(int j=0; j<par.width; j++){
                val = cellgrid[i][j];
                if(val==0){
                    printf("%s", "_");
                }else{
                    // // printf("%3.f", val);
                    // if(cells[int(val)-1]->growtype==0){
                    //     printf("%3s", ud);
                    // }else if (cells[int(val)-1]->growtype==1)
                    // {
                    //     printf("%3s", l);
                    // }else
                    // {printf("%3s", r);}

                    // printf("%s", cells[val]->growtype);
                    if(abs(val)<401){
                        printf("%s", "#");
                    }else{
                        printf("%s", "%");
                    }
                    
                }
            }
            printf("\n");
        }
    }
    // if(option=="nutrients"){
    //     for(int i=par.height-1; i>=0; i--){
    //         for(int j=0; j<par.width; j++){
    //             idx = abs(cellgrid[i][j]);
    //             if(idx>0){
    //                 val = cells[idx-1]->nutrients[0];
    //                 printf("%3.f ", val);
    //             }else{
    //                 printf("%3s ", skip);
    //             }

    //         }
    //         printf("\n");
    //     }
    // }
    // if(option=="energy"){
    //     for(int i=par.height-1; i>=0; i--){
    //         for(int j=0; j<par.width; j++){
    //             idx = abs(cellgrid[i][j]);
    //             if(idx>0){
    //                 val = cells[idx-1]->nutrients[1];
    //                 printf("%3.f ", val);
    //             }else{
    //                 printf("%3s ", skip);
    //             }
    //         }
    //         printf("\n");
    //     }
    // }
}

template class Environment<Agent>; //this is to make sure not everything breaks
